import numpy as np
import pandas as pd

from sklearn.preprocessing import OneHotEncoder
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split

from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier

from sklearn.metrics import balanced_accuracy_score, accuracy_score, confusion_matrix

from flask import Flask, request, jsonify, make_response
from flask_restplus import Api, Resource, fields
from sklearn.externals import joblib

flask_app = Flask(__name__)

app = Api(app = flask_app, 
		  version = "1.0", 
		  title = "ML React App", 
		  description = "Predict results using a trained model")

_df = None
_classifier = None

_allCat = ["job", "marital", "education", "default", "housing", "loan",
			"contact", "month", "day_of_week", "poutcome"]

_allNum = ["age", "campaign", "pdays", "duration", "previous", "emp.var.rate",
			"cons.price.idx", "cons.conf.idx", "euribor3m", "nr.employed"]

# --------------------
# ----- Loading ------
# --------------------
# Initial data loading. Returns a matrix of data points
@app.route("/dataLoading")
class DataLoading(Resource):
	def options(self):
		response = make_response()
		response.headers.add("Access-Control-Allow-Origin", "*")
		response.headers.add('Access-Control-Allow-Headers', "*")
		response.headers.add('Access-Control-Allow-Methods', "*")
		return response

	def post(self):
		global _df
		_df = pd.read_csv("../data/bank-additional-full.csv", sep=';')
		_df["y"] = (_df["y"] == "yes").astype(int) 

		try: 
			response = jsonify({
				"statusCode": 200,
				"status": "Got Data",
				"result": _df.to_json(orient="split")
				})
			response.headers.add('Access-Control-Allow-Origin', '*')
			return response
		except Exception as error:
			return jsonify({
				"statusCode": 500,
				"status": "Could not make prediction",
				"error": str(error)
			})

# ---------------------
# ---- Correlation ----
# ---------------------
# Accecpts a list of features and computes the correlation coefficients between
# those features using pandas .corr() function. Pearson's r. Categorical data
# is one hot encoded.
# Returns a matrix of correlation coeficients
@app.route("/correlation")
class Prediction(Resource):
	def options(self):
		response = make_response()
		response.headers.add("Access-Control-Allow-Origin", "*")
		response.headers.add('Access-Control-Allow-Headers', "*")
		response.headers.add('Access-Control-Allow-Methods', "*")
		return response

	def getCorrelations(self, df, featured):
		return df[features].corr()

	def post(self):

		request_arr = np.asarray(request.json)

		features = request_arr

		correlations = []

		if len(features) > 0:

			myDf = pd.DataFrame()
			for f in features:
				if f in _allCat:
					myDf = pd.concat([myDf, pd.get_dummies(_df[f], prefix=f)], axis=1)
				else:
					myDf = pd.concat([myDf, pd.DataFrame(_df[f])], axis=1)

			correlations = myDf.corr()

			# remove correlations between one-hot-encoded features of the same type 
			for f in features:
				if f in _allCat:
					thisCols = [col for col in correlations.columns if (f+"_") in col]
					correlations.loc[thisCols, thisCols] = 0.0
					for c in thisCols:
						correlations.loc[c,c] = 1.0

		try: 
			response = jsonify({
				"statusCode": 200,
				"status": "Got Data",
				"result": correlations.to_json(orient="split")
				})

			response.headers.add('Access-Control-Allow-Origin', '*')
			response.headers.add('Access-Control-Allow-Headers', '*')
			response.headers.add('Access-Control-Allow-Methods', '*')
			return response
		except Exception as error:
			return jsonify({
				"statusCode": 500,
				"status": "Could not make prediction",
				"error": str(error)
			})


# --------------------
# ---- Prediction ----
# --------------------
# Accepts a list of features and a model. The model will be trained with
# respective features and an accuracy score of the test set is beeing returned.
# The classifier can be accessed at the global _classifier 
@app.route("/prediction")
class Prediction(Resource):
	def options(self):
		response = make_response()
		response.headers.add("Access-Control-Allow-Origin", "*")
		response.headers.add('Access-Control-Allow-Headers', "*")
		response.headers.add('Access-Control-Allow-Methods', "*")
		return response

	def runModel(self, df, features, selectedModel):
		global _classifier

		num_features = []
		cat_features = []

		for f in features:
			if f in _allCat:
				cat_features.append(f)
			else:
				num_features.append(f)

		preprocessor = ColumnTransformer([
					("numerical", StandardScaler(), num_features), 
                    ("categorical", OneHotEncoder(sparse=False, handle_unknown="ignore"), cat_features)])
		
		if selectedModel == "logisticRegression":
			_classifier = Pipeline([("preprocessor", preprocessor), 
						("model", LogisticRegression(class_weight="balanced", random_state=42))])
		elif selectedModel == "decisionTree": 
			_classifier = Pipeline([("preprocessor", preprocessor), 
						("model", DecisionTreeClassifier(max_depth=8, min_samples_split=200, class_weight="balanced", random_state=42))])
		else:
			_classifier = Pipeline([("preprocessor", preprocessor), 
						("model", RandomForestClassifier(max_depth=8, min_samples_split=50, n_estimators = 200, class_weight="balanced", random_state=42))])

		X = df[features]
		y = df["y"]

		X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

		_classifier.fit(X_train, y_train)
		y_test_predict = _classifier.predict(X_test)
		y_train_predict = _classifier.predict(X_train)

		test_acc = balanced_accuracy_score(y_test, y_test_predict)
		train_acc = balanced_accuracy_score(y_train, y_train_predict)

		return (test_acc, train_acc)

	def post(self):

		request_arr = np.asarray(request.json)

		selectedModel = request_arr[0]
		features = request_arr[1]

		test_acc = 0
		train_acc = 0
		if len(features) > 0:
			test_acc, train_acc = self.runModel(_df, features, selectedModel)

		result = [test_acc, train_acc]

		try: 
			response = jsonify({
				"statusCode": 200,
				"status": "Got Data",
				"result": result
				})

			response.headers.add('Access-Control-Allow-Origin', '*')
			response.headers.add('Access-Control-Allow-Headers', '*')
			response.headers.add('Access-Control-Allow-Methods', '*')
			return response
		except Exception as error:
			return jsonify({
				"statusCode": 500,
				"status": "Could not make prediction",
				"error": str(error)
			})
