import React, { Component } from 'react'
import './Histo.css'


/*
    Class for visualizing distributions. Data can be split up
    depending on the target variable. Distributions can be normalized.
*/
class BarChart extends Component {

    constructor(props){
        super(props)
        this.createBarChart = this.createBarChart.bind(this)
    }   

    componentDidMount() {
        this.createBarChart()
    }   

    componentDidUpdate() {
        this.createBarChart()
    }

    createBarChart() {
        const SVGnode = this.SVGnode
        const pad = this.pad
        const xAxis = this.xAxis
        const yAxis = this.yAxis
        const legend = this.legend

        const xAxis_title = this.xAxis_title
        const yAxis_title = this.yAxis_title

        const dataObj = this.props.data
        const col = this.props.column

        const BINS = 50 // number of bins in the histogram

        const categoricalColumns = ["job", "marital", "education", "default", "housing", "loan", "contact", "month", "day_of_week", "poutcome", "y"]
        var isCat = categoricalColumns.includes(col)
        var isSplit = this.props.split
        var isNormalize = this.props.normalize

        var margin = {top: 30, right: 40, bottom: 110, left: 70},
            width = 500 - margin.left - margin.right,
            height = 450 - margin.top - margin.bottom;

        const d3 = require("d3");

        d3.select(SVGnode)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        
        d3.select(pad)
                .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        var x = null;
        var y = null;
        var bins = {};

        var data = {}

        // prepare data sets
        if(isSplit) {
            data.no = dataObj.data.filter( (x, i) => dataObj.y[i] === 0);
            data.yes = dataObj.data.filter( (x, i) => dataObj.y[i] === 1);
        } else {
            data.total = dataObj.data;
            data.dummy = []; // necessary so that the second bars get removed when switched to "total"
        }

        if(isCat) {
            var allData = dataObj.data

            // getting an array of unique categories using sets
            var catsSet = [...new Set(allData)];
            let allBins = [] // necessary to calculate y-domain

            // generate bins for an abitrary amount of data sets
            for (const [key, value] of Object.entries(data)) {
                bins[key] = []

                // empty template of bins
                catsSet.forEach(category => {
                    bins[key].push({cat: category, count: 0})
                });

                // count occurances
                value.forEach(c => {
                    const found = bins[key].find(e => e.cat === c)
                    found.count = found.count + 1
                });

                // normalization of bins
                let norm = 1;
                if(isNormalize) {
                    const reducer = (acc, curr) => acc + parseFloat(curr.count)
                    norm = bins[key].reduce(reducer, 0);
                    console.log(norm)

                    if(!(norm===0)){
                        bins[key].forEach(e => e.count = e.count/norm)
                    }
                }

                allBins = allBins.concat(bins[key])
            }

            // define scales
            x = d3.scaleBand()
                .domain(Array.from(catsSet))
                .range([0,width])
                .padding(0.1) 

            y = d3.scaleLinear()
                .domain([0, d3.max(allBins.map(d => d.count))])
                .range([height, 0]);
            
            console.log(bins)
        } else {
            x = d3.scaleLinear()
                .domain([d3.min(dataObj.data), d3.max(dataObj.data)])     
                .range([0, width]);

            var histogram = d3.histogram()
                .domain(x.domain())
                .thresholds(x.ticks(BINS));

            // generate bins for an abitrary amount of data sets
            let allBins = [] // necessary to calculate y-domain
            for (const [key, value] of Object.entries(data)) {
                let hist = histogram(value)
                bins[key] = hist.map(e => {return {count: e.length, x0: e.x0, x1: e.x1}})

                let norm = 1;
                if(isNormalize) {
                    const reducer = (acc, curr) => acc + curr.count
                    norm = bins[key].reduce(reducer, 0);
                    console.log(norm)

                    if(!(norm===0)){
                        bins[key].forEach(e => e.count = e.count/norm)
                    }
                }

                allBins = allBins.concat(bins[key])
            }
            y = d3.scaleLinear()
                .domain([0, d3.max(allBins, function(d) { return d.count; })])
                .range([height, 0]);
        }

        // axes
        d3.select(xAxis)
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x))
            .attr("class", "axis");

        if(isCat) {
            d3.select(xAxis)
                .selectAll("text")
                    .attr("transform", "rotate(-45)")
                    .style("text-anchor", "end");
        }
        
        d3.select(xAxis_title)
            .attr("transform",
                "translate(" + (width/2) + " ," + 
                    100 + ")")
            .attr("class", "xAxis_title")
            .style("text-anchor", "middle")
            .text(col);

        d3.select(yAxis_title)
            .attr("transform",
                "translate(" + -5 + " ," + 
                    -10 + ")")
            .attr("class", "yAxis_title")
            .text("Counts");

        d3.select(yAxis)
            .call(d3.axisLeft(y))
            .attr("class", "axis");

        // creating bars in different colors for different data sets
        var colors = ["rgb(36, 124, 240)", "orange"];
        var i = 0;
        for (const [key, value] of Object.entries(bins)) {
            d3.select(pad)
                .selectAll('rect.histoBars'+i)
                .data(value)
                .enter()
                .append('rect')
                    .attr("class", "histoBars"+i);

            d3.select(pad)
                .selectAll('rect.histoBars'+i)
                .data(value)
                .exit()
                .remove();

            if(isCat) {
                d3.select(pad)
                    .selectAll("rect.histoBars"+i)
                    .data(Array.from(value))
                    .attr("x", 1)
                    .attr("transform", function(d) { return "translate(" + x(d.cat) + "," + y(d.count) + ")"; })
                    .attr("width", x.bandwidth() )
                    .attr("height", function(d) { return height - y(d.count); })
                    .style('fill', colors[i])
                    .style("opacity", 0.6);
            } else {
                d3.select(pad)
                    .selectAll("rect.histoBars"+i)
                    .data(value)
                    .attr("x", 1)
                    .attr("transform", function(d) { return "translate(" + x(d.x0) + "," + y(d.count) + ")"; })
                    .attr("width", function(d) { return x(d.x1) - x(d.x0) -1 ; })
                    .attr("height", function(d) { return height - y(d.count); })
                    .style('fill', colors[i])
                    .style("opacity", 0.6);
            }
            
            i = i+1
        }

        // build legend
        if(isSplit) {
            d3.selectAll(".legendElement").remove()
            d3.select(legend).attr("transform","translate(340,-5)").attr("class", "legend")
            d3.select(legend).append("rect").attr("x",-20).attr("y",-15).attr("width", 90).attr("height",75).attr("rx",10).attr("stroke","black").attr("stroke-width",1).style("fill", "white").style("opacity", 0.7).attr("class", "legendElement")
            d3.select(legend).append("text").attr("x", -8).attr("y", 3).text("success").style("font-size", "15px").style("font-weight", "bold").attr("alignment-baseline","middle").attr("class", "legendElement")
            d3.select(legend).append("circle").attr("cx",0).attr("cy",20).attr("r", 6).style("fill", colors[1]).attr("class", "legendElement")
            d3.select(legend).append("circle").attr("cx",0).attr("cy",45).attr("r", 6).style("fill", colors[0]).attr("class", "legendElement")
            d3.select(legend).append("text").attr("x", 20).attr("y", 23).text("Yes").style("font-size", "15px").attr("alignment-baseline","middle").attr("class", "legendElement")
            d3.select(legend).append("text").attr("x", 20).attr("y", 48).text("No").style("font-size", "15px").attr("alignment-baseline","middle").attr("class", "legendElement")
            d3.select(legend).raise()
        } else {
            d3.selectAll(".legendElement").remove()
        }
    } 
   
   render() {
      return <svg ref={SVGnode => this.SVGnode = SVGnode}
      width={500} height={500}>
        <g ref={pad => this.pad = pad}>
            <g ref={xAxis => this.xAxis = xAxis}>
                <text ref={xAxis_title => this.xAxis_title = xAxis_title}></text>
            </g>
            <g ref={yAxis => this.yAxis = yAxis}>
                <text ref={yAxis_title => this.yAxis_title = yAxis_title}></text>
            </g>
            <g ref={legend => this.legend = legend}></g>
        </g>
      </svg>
   }
}
export default BarChart