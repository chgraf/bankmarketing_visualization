import React, { Component } from 'react';
import './correlation.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select'

import CorrelationHeatMap from './CorrelationHeatMap.js'

import 'bootstrap/dist/css/bootstrap.css';


class Correlation extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      featureSelect: [{value: "loading", label:'Loading...'}],
      selectedColumns: [],
      correlationData: null,
      correlationColumns: null,
      result: ""
    };

    let cols = this.props.dataColumns;

    this.state.featureSelect = cols.map(e => {return {value: e, label: e}});
  }

  handleSelectFeatureChange = (event) => {
    let select = (Array.isArray(event) ? event.map(x => x.value) : [])
    this.setState({selectedColumns: select});
  }

  handleGetCorrelationsClick = (event) => {
    this.setState({isLoading: true})

    // send Request
    fetch('http://127.0.0.1:5000/correlation',
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify(this.state.selectedColumns)
      })
      .then(response => response.json())
      .then(response => {
        let res = JSON.parse(response.result);

        this.setState({
          correlationData: res.data,
          correlationColumns: res.columns,
          isLoading: false
        });
      });

  }

  render() {
    let isLoading = this.state.isLoading;
    let featureSelect = this.state.featureSelect;
    let selectedColumns = this.state.selectedColumns;
    let correlationColumns = this.state.correlationColumns;
    let correlationData = this.state.correlationData;

    return (
      <div class="correlationContent">
        <Form>
          <Row>
            <Col>
              <div class="featureMultiSelectContainer">
                <Select options={featureSelect} 
                        value={featureSelect.filter(obj => selectedColumns.includes(obj.value))}
                        onChange={this.handleSelectFeatureChange}
                        isMulti 
                        name="featureSelect" 
                        placeholder="Select Features..."
                        closeMenuOnSelect={false}/>
              </div>
            </Col>
            <Col>
                <Button
                  block
                  variant="success"
                  disabled={isLoading}
                  onClick={!isLoading ? this.handleGetCorrelationsClick : null}
                  >
                  { isLoading ? 'Loading...' : 'Calculate Correlations' }
                </Button>
            </Col>
          </Row>
          <Row>
            <Col>
            {correlationData === null ? <label> Select Features </label> : ( 
              <CorrelationHeatMap data={correlationData} columns={correlationColumns}/>
            )}
            </Col>
          </Row>
          <Row>
            <Col>
              <div>{this.state.result}</div>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Correlation;
