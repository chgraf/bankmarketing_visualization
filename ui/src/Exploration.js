import React, { Component } from 'react';
import './exploration.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import 'bootstrap/dist/css/bootstrap.css';

import BarChart from './Histo.js'

import { COLUMN_INFO } from './constants.js'

class Exploration extends Component {

  constructor(props) {
    super(props);

    let data = this.props.data

    this.state = {
      splitRadio: "total",
      normalizeCheckBox: false,
      selectedDistribution: {data: data[0], y: data[data.length-1]},
      selectedColumn: "age",
      selectedColumnInfo: COLUMN_INFO["age"]
    };
  }

  handleColumnSelectChange = (event) => {
    var col = this.props.dataColumns.indexOf(event.target.value);
    var df = this.props.data;
    this.setState({selectedDistribution: {data: df[col], y: df[df.length-1]},
                   selectedColumn: event.target.value,
                   selectedColumnInfo: COLUMN_INFO[event.target.value]});
  }

  handleSplitRadioChange = (event) => {
    this.setState({splitRadio: event.target.value });
  }

  handleNormalizeCheckBoxChange = (event) => {
    this.setState({normalizeCheckBox: event.target.checked});
  }

  render() {
    const selectedDistribution = this.state.selectedDistribution;
    const selectedColumn = this.state.selectedColumn;
    const splitRadio = this.state.splitRadio;
    const normalizeCheckBox = this.state.normalizeCheckBox;
    const selectedColumnInfo = this.state.selectedColumnInfo;

    return (
      <div class="explorationContent">
        <Form>
          <Row>
            <Col className="histo-container">
              <BarChart data={selectedDistribution} column={selectedColumn} split={splitRadio === "split"} normalize={normalizeCheckBox} size={[500,500]} />
            </Col>
            <Col>
              <Row>
                <Form.Group>
                  <Form.Label>Select Column</Form.Label>
                  <Form.Control 
                    as="select"
                    value={selectedColumn}
                    name="select1"
                    onChange={this.handleColumnSelectChange}>
                        {this.props.dataColumns.map((x,i) => 
                          <option key={i}>{x}</option>
                        )};
                  </Form.Control>
                </Form.Group>
              </Row>
              <Row>
                <Col>
                  <Form.Group >
                      <Form.Check
                        checked={this.state.splitRadio === "total"}
                        id="radioButtonSplitForm1"
                        type="radio" 
                        name="splitRadio"
                        value="total"
                        label="Total"
                        onClick={this.handleSplitRadioChange} />
                      <Form.Check 
                        checked={this.state.splitRadio === "split"}
                        id="radioButtonSplitForm2"
                        type="radio" 
                        name="splitRadio"
                        value="split"
                        label="Split y"
                        onClick={this.handleSplitRadioChange} />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="checkBoxNormalizeForm">
                    <Form.Check 
                      checked={this.state.normalizeCheckBox}
                      type="checkbox" 
                      name="normalizeCheckBox"
                      value="normalize"
                      label="Normalize"
                      onClick={this.handleNormalizeCheckBoxChange} />
                  </Form.Group>
                </Col>
              </Row>
            </Col>
          </Row>
          <Row>
            <Col>
              <div class="columnInfoContainer">
                <h5>Info: </h5>
                <div class="columnInfoText">
                  {selectedColumnInfo}
                </div>
              </div>
            </Col>
          </Row>
        </Form>
      </div>
    );
  }
}

export default Exploration;
