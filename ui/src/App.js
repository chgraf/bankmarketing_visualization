import React, { Component } from 'react';
import './App.css';
import Container from 'react-bootstrap/Container';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab'
import 'bootstrap/dist/css/bootstrap.css';

import './constants.js'
import Exploration from "./Exploration.js"
import Correlation from "./Correlation.js"
import Prediction from "./Prediction.js"

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: true,
      dataColumns: [""],
      df: [[0],[0]],
    };

    // Initial Load of data
    fetch('http://127.0.0.1:5000/dataLoading',
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: null
      })
      .then(response => response.json())
      .then(response => {
        var data = JSON.parse(response.result).data;
        data = data[0].map((col, i) => data.map(row => row[i])); // transpose

        // setting columns and data
        this.setState({
          dataColumns: JSON.parse(response.result).columns,
          df: data,
          isLoading: false
        });
      }
    );
  }

  render() {

    return (
      <Container>
        <div>
          <h1 className="title">Bank Marketing Visualization</h1>
        </div>
        <div className="content">
        <Tabs defaultActiveKey="exploration" id="AppTabs">
          <Tab eventKey="exploration" title="Exploration">
            {this.state.isLoading ? <label> Loading... </label> : ( 
              // build component only after initial data loading is finished
              <Exploration data={this.state.df} dataColumns={this.state.dataColumns} />
            )}
          </Tab>
          <Tab eventKey="correlation" title="Correlation">
            {this.state.isLoading ? <label> Loading... </label> : (
              <Correlation data={this.state.df} dataColumns={this.state.dataColumns} />
            )}
          </Tab>
          <Tab eventKey="prediction" title="Prediction">
            {this.state.isLoading ? <label> Loading... </label> : (
              <Prediction dataColumns={this.state.dataColumns}/>
            )}
          </Tab>
        </Tabs>
        </div>
      </Container>
    );
  }
}

export default App;
