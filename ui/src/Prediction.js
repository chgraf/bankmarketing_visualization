import React, { Component } from 'react';
import './prediction.css';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';
import Button from 'react-bootstrap/Button';
import Select from 'react-select'

import 'bootstrap/dist/css/bootstrap.css';

class Prediction extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isTraining: false,
      featureSelect: [{value: "loading", label:'Loading...'}],
      selectedColumns: [],
      selectedModel: "logisticRegression",
      result: "",
      testAccuracy: 0,
      trainAccuracy: 0,
      experimentNr: 0,
      history: []
    };

    let cols = this.props.dataColumns.filter(e => e !== "y");
    this.state.featureSelect = cols.map(e => {return {value: e, label: e}});
  }

  handleSelectModelChange = (event) => {
    this.setState({selectedModel: event.value});
  }

  handleSelectFeatureChange = (event) => {
    let select = (Array.isArray(event) ? event.map(x => x.value) : [])
    this.setState({selectedColumns: select});
  }

  handleTrainClick = (event) => {
    this.setState({isTraining: true})

    // send Request
    fetch('http://127.0.0.1:5000/prediction',
      {
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        method: 'POST',
        body: JSON.stringify([this.state.selectedModel, this.state.selectedColumns])
      })
      .then(response => response.json())
      .then(response => {
        // setting columns and data
        let testAcc = (parseFloat(response.result[0])*100).toFixed(2);
        let trainAcc = (parseFloat(response.result[1])*100).toFixed(2);
        this.setState({
          isTraining: false,
          testAccuracy: testAcc,
          trainAccuracy: trainAcc,
          history: this.state.history.concat({id: this.state.experimentNr, 
                    features: this.state.selectedColumns,
                    model: this.state.selectedModel,
                    accuracy: testAcc}),
          experimentNr: this.state.experimentNr + 1
        });
      });
  }


  render() {
    let isTraining = this.state.isTraining;
    let featureSelect = this.state.featureSelect;
    let selectedColumns = this.state.selectedColumns;
    let selectedModel = this.state.selectedModel;
    let testAccuracy = this.state.testAccuracy;
    let trainAccuracy = this.state.trainAccuracy;

    const modelSelect = [{value: "logisticRegression", label:'Logistic Regression'},
                       {value: "decisionTree", label:'Decision Tree'},
                       {value: "randomForest", label:'Random Forest'}]

    return (
      <div class="predictionContent">
        <Form>
          <Row>
            <Col>
              <div class="featureMultiSelectContainer">
                <Select options={featureSelect} 
                        value={featureSelect.filter(obj => selectedColumns.includes(obj.value))}
                        onChange={this.handleSelectFeatureChange}
                        isMulti 
                        name="featureSelect" 
                        placeholder="Select Features..."
                        closeMenuOnSelect={false}/>
              </div>
            </Col>
            <Col>
            </Col>
          </Row>
          <Row>
            <Col>
              <Select options={modelSelect}
                      value={modelSelect.filter(obj => selectedModel.includes(obj.value))}
                      onChange={this.handleSelectModelChange}
                      name="modelSelect" 
                      placeholder="Select Model..."
                      closeMenuOnSelect={true}/>
            </Col>
            <Col>
                <Button
                  block
                  variant="success"
                  disabled={isTraining}
                  onClick={!isTraining ? this.handleTrainClick : null}
                  >
                  { isTraining ? 'Training...' : 'Train' }
                </Button>
            </Col>
            <Col>
              {testAccuracy === 0 ? null :
                <div>
                  <h5 id="train_result">{"Train Accuracy: "+trainAccuracy+"%"}</h5>
                  <h5 id="test_result">{"Test Accuracy: "+testAccuracy+"%"}</h5>
                </div>
              }
            </Col>
          </Row>
          <div class="historyContainer">
            <h5> History </h5>
              <div class="historyHeadings">
                <Row>
                  <Col>ID</Col>
                  <Col>Features</Col>
                  <Col>Model</Col>
                  <Col>Test Accuracy</Col>
                </Row>
              </div>
            { this.state.history.map(h => 
              <Row>
                <Col>{h.id}</Col>
                <Col>{h.features.join(", ")}</Col>
                <Col>{modelSelect.filter(obj => h.model === obj.value)[0].label}</Col>
                <Col>{h.accuracy} %</Col>
              </Row>
              )
            }
          </div>
        </Form>
      </div>
    );
  }
}

export default Prediction;
