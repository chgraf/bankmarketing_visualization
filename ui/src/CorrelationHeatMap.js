import React, { Component } from 'react'
import Container from 'react-bootstrap/Container';
import './CorrelationHeatMap.css'

/*
    Class for visualizing correlations between features
    as a heatmap. Including a tooltip showing tha value 
    of a cell
*/
class CorrelationHeatMap extends Component {

    constructor(props){
        super(props)
        this.createHeatMap = this.createHeatMap.bind(this)
    }   

    componentDidMount() {
        this.createHeatMap()
    }   

    componentDidUpdate() {
        this.createHeatMap()
    }

    createHeatMap() {
        const SVGnode = this.SVGnode;
        const pad = this.pad;

        const tooltipDiv = this.tooltipDiv;

        const xAxis = this.xAxis
        const yAxis = this.yAxis

        const data = this.props.data;
        const columns = this.props.columns;

        var margin = {top: 30, right: 30, bottom: 120, left: 120},
            width = 600 - margin.left - margin.right,
            height = 600 - margin.top - margin.bottom;

        const d3 = require("d3");

        d3.select(SVGnode)
            .attr("width", width + margin.left + margin.right)
            .attr("height", height + margin.top + margin.bottom)
        
        d3.select(pad)
                .attr("transform",
                  "translate(" + margin.left + "," + margin.top + ")");

        console.log(columns)
        console.log(data)

        let dataList = []
        for(var i=0; i < columns.length; i++) {
            for(var j=0; j < columns.length; j++) {
                dataList.push({"x": columns[i], "y": columns[j], "value": data[i][j]});
            }
        }

        // Build X scales and axis:
        var x = d3.scaleBand()
            .range([ 0, width ])
            .domain(columns)
            .padding(0.05);

        d3.select(xAxis)
            .attr("transform", "translate(0," + height + ")")
            .call(d3.axisBottom(x).tickSize(0))
            .select(".domain").remove()

        d3.select(xAxis)
            .selectAll("text")
                .attr("transform", "rotate(-45)")
                .style("text-anchor", "end")

        // Build Y scales and axis:
        var y = d3.scaleBand()
            .range([ height, 0 ])
            .domain(columns.reverse())
            .padding(0.05);
        d3.select(yAxis)
            .call(d3.axisLeft(y).tickSize(0))
            .select(".domain").remove()

        // Build color scale
        var myColor = d3.scaleSequential()
            .interpolator(d3.interpolateRdBu)
            .domain([1,-1])

        // create a tooltip
        var tooltip = d3.select(tooltipDiv)
            .style("opacity", 0)
            .attr("class", "tooltip")
            .style("background-color", "white")
            .style("border", "solid")
            .style("border-width", "2px")
            .style("border-radius", "5px")
            .style("padding", "5px");

        // Three function that change the tooltip when user hover / move / leave a cell
        var mouseover = function(d) {
            tooltip
            .style("opacity", 1)
            d3.select(this)
            .style("stroke", "black")
            .style("opacity", 1)
        }
        var mousemove = function(d) {
            let val = d.target.__data__.value.toFixed(3)
            let col1 = d.target.__data__.x
            let col2 = d.target.__data__.y

            tooltip
            .html("<b>"+ col1 + "</b>, <b>" + col2 + "</b><br>Correlation: <b>" + val + "</b>")
            .style("left", (d.layerX+50) + "px")
            .style("top", (d.layerY) + "px")
        }
        var mouseleave = function(d) {
            tooltip
            .style("opacity", 0)
            d3.select(this)
            .style("stroke", "none")
            .style("opacity", 0.8)
        }

        d3.select(pad)
            .selectAll('rect')
            .data(dataList)
            .enter()
            .append('rect')

        d3.select(pad)
            .selectAll('rect')
            .data(dataList)
            .exit()
            .remove();


        // add the squares
        d3.select(pad)
            .selectAll('rect')
            .data(dataList)
            .attr("x", function(d) { return x(d.x) })
            .attr("y", function(d) { return y(d.y) })
            .attr("rx", 4)
            .attr("ry", 4)
            .attr("width", x.bandwidth() )
            .attr("height", y.bandwidth() )
            .style("fill", function(d) { return myColor(d.value)} )
            .style("stroke-width", 4)
            .style("stroke", "none")
            .style("opacity", 0.8)
            .on("mouseover", mouseover)
            .on("mousemove", mousemove)
            .on("mouseleave", mouseleave)
    } 
   
   render() {
      return (
        <Container>
         <div ref={container => this.container = container}> 
            <svg ref={SVGnode => this.SVGnode = SVGnode}
                width={500} height={500}>
                <g ref={pad => this.pad = pad}>
                    <g class="axis" ref={xAxis => this.xAxis = xAxis}>
                        <text ref={xAxis_title => this.xAxis_title = xAxis_title}></text>
                    </g>
                    <g class="axis" ref={yAxis => this.yAxis = yAxis}>
                        <text ref={yAxis_title => this.yAxis_title = yAxis_title}></text>
                    </g>
                </g>
            </svg>
            <div ref={tooltipDiv => this.tooltipDiv = tooltipDiv}> </div>

        </div>
        </Container>
      );
   }
}
export default CorrelationHeatMap